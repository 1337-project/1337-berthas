import { createRouter, createWebHistory } from "vue-router";
import HomeLayout from "@/layouts/HomeLayout.vue";
import UploadLayout from "@/layouts/UploadLayout.vue";

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
        {
            path: "/",
            name: "home",
            component: HomeLayout,
        },
        {
            path: "/upload",
            name: "upload",
            component: UploadLayout,
        },
    ],
});

export default router;
