/**
 * main.js
 *
 * Bootstraps Vuetify and other plugins then mounts the App`
 */
import { createApp } from 'vue';
import { createPinia } from 'pinia';
import App from '@/App.vue'
import router from '@/router';
import './sass/app.scss';

const pinia = createPinia();
import { registerPlugins } from '@/plugins'

const app = createApp(App)
    .use(pinia)
    .use(router);

registerPlugins(app)

app.mount('#app')
